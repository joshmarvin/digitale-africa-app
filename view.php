<?php require("config/db.php"); ?>
<!DOCTYPE html>
<html>
<title>Voir nos devoirs ou une interros</title>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <style>
    #content {
        display: block;
    }

    .internal {
        display: block;
    }
    </style>
</head>

<body>
    <div class="container external internal" id="content">

        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="index.php">Ajouter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Voir la galerie</a>
            </li>
        </ul>
        <div class="card mt-4">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="mb-12">
                            <div class="alert alert-info" role="alert">
                                Hello, notre plateforme vous permet d'uploader et voir en image les interros et devoirs
                                de
                                classe
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="list-group">
                                <!-- <a href="#" class="list-group-item list-group-item-action active" aria-current="true">
                                The current link item
                            </a> -->
                                <?php
                                $query = $conn->prepare("SELECT * FROM examens");
                                $query->execute();
                                while($row = $query->fetch()){
                                    ?>
                                <a href="view.php?id=<?php echo $row['id']?>"
                                    class="list-group-item list-group-item-action <?php echo $row['id'] == $_GET['id'] ? 'active' : ''?> "><?php echo $row['nom']?></a>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-8">
                        <?php
                        if(isset($_GET['id'])){
                            ?>
                        <div class="card">
                            <div class="card-body">
                                <?php
                            if(isset($_GET['id'])){

                                $query = "SELECT * FROM examens WHERE id = :id";
                                $statement = $conn->prepare($query);
                                $statement->bindParam(":id", $_GET['id']);
                                $statement->execute();
                                $row = $statement->fetch();
                                echo "<strong><u>Libele:</u></strong> " . $row['nom'];
                                echo "<br>";
                                echo "<strong><u>Description:</u></strong> ";
                                echo "<br>";
                                echo $row['content'];
                            }
                                ?>
                            </div>
                        </div>
                        <div class="card mt-2">
                            <div class="card-body">
                                <?php
                            if(isset($_GET['id'])){

                                $query = "SELECT * FROM files WHERE exam_id = :id";
                                $statement = $conn->prepare($query);
                                $statement->bindParam(":id", $_GET['id']);
                                $statement->execute();
                                $rows = $statement->fetchAll();

                                foreach ($rows as $row) {
                                    ?>
                                <div class="col-sm-6 col-md-4">
                                    <div class="thumbnail">
                                        <p align="center"><a href="uploads/<?php echo $row['url']?>"
                                                target="_blank"><img style="height:128px;"
                                                    src="uploads/<?php echo $row['url']?>"></a></p>
                                    </div>
                                </div>
                                <?php
                                }
                            }
                                ?>
                            </div>
                        </div>
                        <?php
                        } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"> -->
</script>

</html>
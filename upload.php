<?php
	// Database connection
	include("config/db.php");

	$allowd_file_ext = array("jpg", "jpeg", "png");

	
	if(isset($_FILES['file'])){
		$countfiles = count($_FILES['file']['name']);
		for($i=0; $i<$countfiles; $i++){
			$filename = $_FILES['file']['name'][$i];
			// $target_file = 'upload/'.$filename;
			// move_uploaded_file($_FILES['file']['tmp_name'][$i],$target_file);
			// $statement->execute(array($filename,$target_file));
	
			//Le nom du fichier
			// $tmpFilePath = $_FILES['file']['tmp_name'][$i];
	
			//L'extension du fichier
			$imageExt = strtolower(pathinfo($filename, PATHINFO_EXTENSION));
	
			//Verifions si nous avons une extension valide
			if (!in_array($imageExt, $allowd_file_ext)) {
				$resMessage = array(
					"status" => "alert-danger",
					"message" => "Fichier autorisés .jpg, .jpeg et .png."
				);
			}
		}
	}


	// Si nous avons une erreur on sort de ce code
	if(!isset($resMessage) && isset($_FILES['file'])){
		// Config de l'emplacement des fichiers
        $target_dir = "uploads/";

		$query = "INSERT INTO examens (nom,content,devoir) VALUES (?,?,?)";
		$statement = $conn->prepare($query);
		$statement->execute(array($_POST['label'],$_POST['desc'],$_POST['radio']));

		$id = $conn->lastInsertId();

		for($i=0; $i<$countfiles; $i++){
			// Nom du fichier
			$filename = $_FILES['file']['name'][$i];
			//Lien du fichier temporaire
			$tmpFilePath = $_FILES['file']['tmp_name'][$i];
			// Lien de sauvegarde
			$target_file = $target_dir . basename($filename);

			$query = "INSERT INTO files (url,exam_id) VALUES(?,?)";
    		$statement = $conn->prepare($query);

			if (move_uploaded_file($tmpFilePath, $target_file)) {
				$statement->execute(array($filename,$id));
			}
		}

		$resMessage = array(
			"status" => "alert-success",
			"message" => "Sauvegarde effectué avec succès."
		);  
	}
?>
<?php include("upload.php"); ?>
<!DOCTYPE html>
<html>
<title>Importer un devoir ou une interro</title>

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <style>
        #content {
            display: block;
        }

        .internal {
            display: block;
        }
    </style>
</head>

<body>
    <div class="container external internal" id="content">

        <ul class="nav nav-pills">
            <li class="nav-item">
                <a class="nav-link" href="#">Ajouter</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="view.php">Voir la galerie</a>
            </li>
        </ul>
        <div class="card mt-4">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <div class="mb-12">
                            <div class="alert alert-info" role="alert">
                                Hello, notre plateforme vous permet d'uploader et voir en image les interros et devoirs
                                de
                                classe
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <form method="POST" action="" enctype="multipart/form-data">
                            <div class="mb-3">
                                Choix du type
                            </div>
                            <div class="mb-3">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="radio" value="0" id="flexRadioDefault2" checked>
                                    <label class="form-check-label" for="flexRadioDefault2">
                                        Interrogation
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="radio" value="1" id="radio1">
                                    <label class="form-check-label" for="radio1">
                                        Devoir de classe
                                    </label>
                                </div>
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlInput" class="form-label">Libele</label>
                                <input class="form-control" id="exampleFormControlInput" placeholder="Example: Exercie 1 - 2nC11 - LCA" name="label" required>
                            </div>
                            <div class="mb-3">
                                <label for="exampleFormControlTextarea1" class="form-label">Description</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" name="desc" rows="3" required></textarea>
                            </div>
                            <div class="mb-3">
                                <label for="formFileMultiple" class="form-label">Importer</label>
                                <input class="form-control" type="file" name="file[]" id="formFileMultiple" multiple>
                            </div>
                            <button type="submit" class="btn btn-primary">Enregistrer</button>
                        </form>
                    </div>
                </div>
                <!-- Display response messages -->
                <?php if (!empty($resMessage)) { ?>
                    <div class="alert <?php echo $resMessage['status'] ?> mt-3">
                        <?php echo $resMessage['message'] ?>
                    </div>
                <?php } ?>
            </div>
        </div>

    </div>
</body>
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
    integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"> -->
</script>

</html>
